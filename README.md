# event-hub-lite

A lightweight schema-enforcing event broker based on easily-tamed components requiring
little specialised maintenance / provision knowledge.
